# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

from .task_progress import TaskProgress

POLICIES = (
    (u'time', u'time'),
    (u'full', u'full')
)

class Trash(models.Model):

    path_trash = models.CharField(max_length=300)
    discription = models.TextField(blank=True)
    capacity = models.IntegerField(blank=True, null=True)
    policy = models.CharField(max_length=20, choices=POLICIES, default=u'Full')


    def __unicode__(self):
        return self.path_trash


class Task(models.Model):

    name = models.CharField(max_length=50, default="Task")
    state = models.IntegerField(default=TaskProgress.CREATED)

    trash = models.ForeignKey(Trash)

    dry_run = models.NullBooleanField()

    regex = models.BooleanField()
    restore = models.BooleanField()

    paths = models.TextField()

    def __unicode__(self):
        return self.name