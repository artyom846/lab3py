from django import forms
from .models import Trash,Task


class TrashForm(forms.ModelForm):
    class Meta:
        model = Trash
        fields = ('path_trash', 'discription', 'capacity', 'policy')


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ('name', 'trash',
                  'dry_run','restore',
                  'regex', 'paths')