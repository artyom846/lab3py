from django.conf.urls import url
from . import views


app_name = "rmapp"
urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^trashview/(?P<trash_id>[0-9]+)$', views.trash_view, name='trashview'),
    url(r'^del/(?P<trash_id>[0-9]+)$', views.rm_file_by_name, name='del'),
    url(r'^trashnew$', views.trash_new, name='trashnew'),
    url(r'^trash_delete/(?P<trash_id>[0-9]+)$',views.trash_delete, name='trash_delete'),
    url(r'^task/new/$', views.task_new, name='task_new'),
    url(r'^task/$', views.task_list, name='task_list'),
    url(r'^task/(?P<pk>\d+)/$', views.task_detail, name='task_detail'),
    url(r'^task/result/(?P<pk>\d+)/$', views.task_result, name='task_result'),
]
