# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import Trash, Task
from django.shortcuts import render, redirect
from .forms import TrashForm, TaskForm
from django.shortcuts import get_object_or_404
from task_progress import TaskProgress
from merm.trash_manager import regular_remove
from merm.trash_manager import remove_list, recover_list

import shutil
import merm.trash_manager
import json
import os
import multiprocessing


# Create your views here.
class File:
    def __init__(self, id, name, path):
        self.id = id
        self.name = name
        self.path = path


def index(request):

    trashes = Trash.objects.all()

    return render(request, 'rmapp/index.html', {"trashes": trashes})


def trash_view(request, trash_id):

    trash = get_object_or_404(Trash, pk=int(trash_id))
    with open(os.path.join(trash.path_trash, "_____111_1"),"r") as file:
        data = json.load(file)

    res = []
    counter = 0

    for key in data:
        res.append(File(counter, key, data[key]))
        counter += 1

    if request.method == "POST":
        result = request.POST.getlist('file')
        for i in result:
            merm.trash_manager.recover(res[int(i)].name, force=True, path = trash.path_trash)

    return render(request, 'rmapp/trashview.html', {'res': res, 'trash': trash})


def trash_new(request):

    form = TrashForm()

    if request.method == "POST":
        form = TrashForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            path = post.path_trash

            if not os.path.exists(path):
                os.mkdir(path)

            if not os.path.exists(os.path.join(path, "_____111_1")):
                with open(os.path.join(path, "_____111_1"), "w") as file:
                    json.dump({}, file)

            post.save()

            return redirect('rmapp:index')
    else:
        form = TrashForm()
    return render(request, 'rmapp/trashnew.html', {'form': form})


def rm_file_by_name(request, trash_id):

    trash = get_object_or_404(Trash, pk=int(trash_id))

    if request.method == "POST":
        result = request.POST
        file = result["text"]
        merm.trash_manager.remove(file,force=True, path=trash.path_trash)
        return redirect('rmapp:index')

    return render(request, 'rmapp/del.html', {})


def trash_delete(request, trash_id):

    trash = get_object_or_404(Trash, pk=int(trash_id))
    shutil.rmtree(trash.path_trash)
    trash.delete()

    return redirect('rmapp:index')


def task_new(request):
    form = TaskForm()

    if request.method == 'POST':
        form = TaskForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('rmapp:task_list')

    return render(request, 'rmapp/task_new.html', {'form': form})


def task_result(request, pk):

    task = Task.objects.get(pk=pk)

    task.state = TaskProgress.WORKING
    task.save()

    context = task_run(task)

    task.state = TaskProgress.COMPLETED
    task.save()

    return render(request, 'rmapp/task_result.html', context)


def task_list(request):

    tasks = {
        'tasks': Task.objects.order_by('name'),
    }

    return render(request, 'rmapp/task_list.html', tasks)


def task_detail(request, pk):

    task = Task.objects.get(pk=int(pk))
    form = TaskForm(instance=task)

    if request.method == "POST":
        form = TaskForm(request.POST, instance=task)

        if form.is_valid:
            form.save()
            return redirect('/task/')

    return render(request, 'rmapp/task_detail.html', {'form': form})


def task_run(task):

    mgr = multiprocessing.Manager()
    result = mgr.list()
    processes = multiprocessing.cpu_count() * 2

    tasks_pathes = task.paths.split()

    if task.restore:
        status = 'Restored'
        process_path = [ [] for i in xrange(processes)]
        for i in xrange(len(tasks_pathes)):
            process_path[i % processes].append(tasks_pathes[i])

        jobs = []
        for i in xrange(processes):
            j = multiprocessing.Process(target=restore_worker,
                                        args=(result, process_path[i], task.dry_run, True, False, task.trash.path_trash))
            jobs.append(j)

        for j in jobs:
            j.start()

        for j in jobs:
            j.join()
    elif task.regex:
        status = 'Regular removed'
        regular_remove(task.paths)
    else:
        status = 'Removed'
        process_path = [[] for i in xrange(processes)]
        for i in xrange(len(tasks_pathes)):
            process_path[i % processes].append(tasks_pathes[i])

        jobs = []
        for i in xrange(processes):
            j = multiprocessing.Process(target=remove_worker,
                                        args=(result,process_path[i], task.dry_run, True, False, task.trash.path_trash, task.trash.policy, task.trash.capacity))
            jobs.append(j)

        for j in jobs:
            j.start()

        for j in jobs:
            j.join()

    context = {
        'result': result,
        'status': status,
    }

    return context


def restore_worker(result, paths, *args, **kwargs):
    result.extend(recover_list(paths, *args, **kwargs))


def remove_worker(result, paths, *args, **kwargs):
    result.extend(remove_list(paths, *args, **kwargs))