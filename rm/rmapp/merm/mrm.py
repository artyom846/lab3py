#module mrm
"""

"""

import sys
import json
import argparse
import config
import logging
from trash_manager import recover,real_remove,regular_remove,recycle_policy
from trash_manager import remove_list

LOGGER = logging.getLogger('application')
CONF = config.Config()


def initialize_logger(silent):
    """
    
    :param silent: 
    :return: 
    """
    LOGGER.setLevel(logging.DEBUG)
    fh = logging.FileHandler(CONF.PATH_LOG)
    ch = logging.StreamHandler()

    fh.setLevel(logging.CRITICAL) if silent else fh.setLevel(logging.DEBUG)
    ch.setLevel(logging.CRITICAL) if silent else ch.setLevel(logging.INFO)

    fileFormatter = logging.Formatter(fmt='# %(asctime)s\n %(levelname)s: %(module)s.%(funcName)s[%(lineno)s] - %(message)s',
                                      datefmt='%Y-%m-%d, %H:%M:%S')
    consoleFormatter = logging.Formatter('%(levelname)s:  %(message)s')
    fh.setFormatter(fileFormatter)
    ch.setFormatter(consoleFormatter)

    LOGGER.addHandler(fh)
    LOGGER.addHandler(ch)


def trash_view():
    """
    The function displays the contents of the recycle bin
    """
    with open(CONF.list_old, "r") as file:
        old_path = json.load(file)

    counter = 1

    for name in sorted(old_path.keys()):
        if counter > 2:
            wait = raw_input("For continue press ENTER, for break press any key")
            if wait == "":
                counter = 1
            else:
                break
        counter += 1
        print name, old_path[name]


def log_view():
    """
    Log output to terminal
    """
    logfile = open(CONF.PATH_LOG, 'r')

    for line in logfile:
        print line.strip()


def parse_argument():
    """
    
    :return: 
    """
    parser = argparse.ArgumentParser()

    parser.add_argument("-l",  dest="recover", help="Recover file")
    parser.add_argument("-c", dest="clean", help="Cleaning the trash")
    parser.add_argument("-dr", dest="dryrun", help="DRYRUN mod",
                        action = "store_const", const=True, default=False)
    parser.add_argument("-v", dest="view", action='store_const',
                        const=True, default=False, help="View the trash")
    parser.add_argument("-log", dest="logview", action='store_const',
                        const=True, default=False, help="View the log" )
    parser.add_argument("-reg",dest="regular", help="delete on regular")
    parser.add_argument("-f", dest="force", help="Ask user",
                        action="store_const", const=True,default=False)
    parser.add_argument("-s", dest="silent",
                        help="Logging and logging to the console",
                        action="store_const", const=True, default=False)
    parser.add_argument("--policy",dest="policy", help="Change policy")
    parser.add_argument("--day", dest="day", help="Change day for policy")
    parser.add_argument("--size", dest="size", help="Change size trash")
    parser.add_argument("-cfg",dest="cfg", help="Cfg format",
                        action="store_const", const=True, default=False)
    parser.add_argument("targets", help="del", nargs="*")
    parser.add_argument("--config", dest="config", help="File config")
    return parser


def Main():
    initialize_logger(silent=False)
    parser = parse_argument()
    namespace = parser.parse_args(sys.argv[1:])
    options = {'json_f':not namespace.cfg}

    if namespace.size:
        options['size'] = int(namespace.size)

    if namespace.day:
        options['day'] = int(namespace.day)

    if namespace.policy:
        options['policy'] = namespace.policy
        print options

    if namespace.config:
        options['path_rm_conf_json'] = namespace.config

    CONF = config.Config(**options)

    if namespace.targets:
        remove_list(namespace.targets, dryrun=namespace.dryrun,
                    force=namespace.force, silent=namespace.silent)

    if namespace.recover:
        recover(namespace.recover, dryrun=namespace.dryrun,
                force=namespace.force, silent=namespace.silent)

    if namespace.clean:
        real_remove(namespace.clean, dryrun=namespace.dryrun,
                    force=namespace.force, silent=namespace.silent)

    if namespace.regular:
        regular_remove(namespace.regular)

    if namespace.view:
        trash_view()

    if namespace.logview:
        log_view()

    recycle_policy(CONF.policy, CONF.day, CONF.size)

if __name__ == '__main__':
    Main()
