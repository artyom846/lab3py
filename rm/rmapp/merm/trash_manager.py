#module trash_manager
"""

"""
import os
import json
import logging
import re
import datetime
import config
import shutil
import time
import random
from ask_user import ask

LOGGER = logging.getLogger('application')
CONF = config.Config()


def remove(name, dry_run=False, force = False, silent = False,path = ''):
    """
    Function deletes the file in the trash
    """
    result = []
    if not path:
        path = CONF.trash
    list_old = os.path.join(path,"_____111_1")
    time.sleep(random.random())
    if os.path.exists(name):
        if dry_run:
            just_name = name.split("/")[-1]
            if not force:
                ask("Are you sure you want to perform " + just_name + " y/yes?")
            if not silent:
                LOGGER.info(just_name + " can be Moved to the trash")
        else:
            just_name = name.split("/")[-1]
            if os.path.exists(list_old):
                with open(list_old, "r") as file:
                    old_path = json.load(file)
                while just_name in old_path:
                    just_name = just_name + "_"
            else:
                old_path = {}
            if not force:
                if ask("Are you sure you want to perform " + just_name + "  y/yes?"):
                    os.rename(name,os.path.join(path,just_name))
                    result.extend((name,os.path.join(path,just_name)))
                    old_path[just_name]= name
                    if not silent:
                        LOGGER.info(just_name + " moved to the trash")
                elif not silent:
                    LOGGER.info(just_name + " don't moved to the trash")
            else:
                os.rename(name,os.path.join(path,just_name))
                result.extend((name, os.path.join(path, just_name)))
                old_path[just_name] = name
                if not silent:
                    LOGGER.info(just_name + " moved to the trash")
            with open(list_old, "w") as file:
                json.dump(old_path,file)
    else:
            LOGGER.error("File not found")
    return result


def remove_list(files, dryrun=False, force=False, silent=False, path='', policy='full', capacity=10000):
    """
    
    :param files: 
    :param dryrun: 
    :param force: 
    :param silent: 
    :return: 
    """
    recycle_policy(policy=policy, size=capacity, day = 6)
    result = []
    for name in files:
        result.append(remove(os.path.abspath(name), dryrun, force, silent,path))
    return result

def recover(name, dryrun=False, force = False, silent = False, path = ''):
    """
    The function restores the file along the old path  
    """

    if not path:
        path = CONF.trash
    listold = os.path.join(path,"_____111_1")
    time.sleep(random.random())
    result = []
    if os.path.exists(listold):
        with open(listold, "r") as file:
            old_path = json.load(file)
        if name in old_path:
            if dryrun:
                if not force:
                    ask("Are you sure you want to perform " + name + " y/yes?")
                if not silent:
                    LOGGER.info(name + " can be recover from the recycle bin")
            else:
                if not force:
                    if ask("Are you sure you want to perform " + name + " y/yes?"):
                        os.rename(os.path.join(path,name), old_path[name])
                        result.extend((os.path.join(path,name), old_path[name]))
                        del(old_path[name])
                        if not silent:
                            LOGGER.info(name + " recover from the recycle bin")
                    elif not silent:
                        LOGGER.info(name + " File not recovered")
                else:
                    os.rename(os.path.join(path, name), old_path[name])
                    result.extend((os.path.join(path, name), old_path[name]))
                    del (old_path[name])
                    if not silent:
                        LOGGER.info(name + " recover from the recycle bin")
            with open(listold, "w") as file:
                json.dump(old_path, file)
        elif not silent:
            LOGGER.error("File not found")
    return result


def recover_list(files, dryrun=False, force = False, silent = False, path = ''):
    result=[]
    for name in files:
        result.append(recover(name, dryrun, force, silent, path))
    return result



def real_remove(name, dryrun=False, force = False, silent = False):
    """
    Function removes the file in full from the recycle bin
    """
    if os.path.exists(CONF.list_old):
        with open(CONF.list_old, "r") as file:
            old_path = json.load(file)
        if name in old_path:
            if dryrun:
                if not force:
                    ask("Are you sure you want to perform " + name + " y/yes?")
                if not silent:
                    LOGGER.info(name + " can be cleared from the recycle bin")
            else:
                if os.path.isfile(os.path.join(CONF.trash,name)) or os.path.islink(os.path.join(CONF.trash,name)):
                    if not force:
                        if ask("Are you sure you want to perform " + name + " y/yes?"):
                            os.remove(os.path.join(CONF.trash,name))
                            del (old_path[name])
                            if not silent:
                                LOGGER.info(name + " clear from trash")
                        else:
                            if not silent:
                                LOGGER.info(name + " don't clear from trash")
                    else:
                        os.remove(os.path.join(CONF.trash,name))
                        del (old_path[name])
                        if not silent:
                            LOGGER.info(name + " clear from trash")
                        else:
                            if not silent:
                                LOGGER.info(name + " don't clear from trash")
                else:
                    if not force:
                        if ask("Are you sure you want to perform " + name + " y/yes?"):
                            shutil.rmtree(os.path.join(CONF.trash,name))
                            del (old_path[name])
                            if not silent:
                                LOGGER.info(name + " clear from trash")
                        elif not silent:
                            LOGGER.info(name + " clear from trash")
            with open(CONF.list_old, "w") as file:
                json.dump(old_path, file)
        else:
            if not silent:
                LOGGER.error("File not found")


def recycle_policy(policy, day, size):
    """
    
    :param policy: 
    :param day: 
    :param size: 
    :return: 
    """
    if policy == "full" or policy == "time":
        if policy == "full":
            real_size = os.path.getsize(CONF.trash)
            if os.path.exists(CONF.list_old):
                with open(CONF.list_old, "r") as file:
                    old_path = json.load(file)
                if real_size>int(size):
                    for name in old_path:
                        real_remove(name, dryrun=False, force=True, silent=False)
        if policy == "time":
            if datetime.datetime.today().isoweekday() == day:
                if os.path.exists(CONF.list_old):
                    with open(CONF.list_old, "r") as file:
                        old_path = json.load(file)
                for name in old_path:
                    real_remove(name, False, True, False)
    else:
        LOGGER.error("ConfigError: configurations have not been changed, is wrong policy")


def regular_remove(pattern):
    """
    Delete function in the regular expression
    """
    reg = os.listdir(".")
    for name in reg:
        if re.findall(pattern, name):
            remove(os.path.abspath(name))

