# module config
"""

"""

import ConfigParser
import json
import os
import logging
import argparse
import sys


class Config(object):
    PATH_RM_CONFIG_JSON = os.path.join(os.path.expanduser('~'),
                                       'Documents/untitled1/rmconfigs.json')
    PATH_RM_CONFIG_CFG = os.path.join(os.path.expanduser('~'), 'Documents/untitled1/rmconfigs.cfg')
    DAY = 6
    SIZE = 1000
    PATH = os.path.join(os.path.expanduser('~'), 'Documents/forlab2/trash')
    POLICY = "full"
    PATH_LOG = os.path.join(os.path.expanduser('~'), 'Documents/forlab2/rmlogs.log')

    def __init__(self, json_f=True, path_rm_conf_json=PATH_RM_CONFIG_JSON,
                 path_conf_cfg=PATH_RM_CONFIG_CFG, policy=POLICY, path=PATH,
                 size=SIZE, day=DAY, path_log=PATH_LOG):

        self.path_rm_conf_json = path_rm_conf_json
        self.path_conf_cfg = path_conf_cfg
        self.standart_configs = {"policy": policy,
                                 "path": path,
                                 "size": size,
                                 "day": day
                                 }

        self.configs = ConfigParser.RawConfigParser()

        if not os.path.exists(self.path_rm_conf_json):
            self.create_configs()

        if not os.path.exists(self.path_conf_cfg):
            self.create_configs()

        self.configs_json = json.load(open(self.path_rm_conf_json, 'r'))
        self.configs.read(self.path_conf_cfg)

        if json_f:
            self.policy = self.configs_json['policy']
            self.trash = self.configs_json['path']
            self.size = self.configs_json['size']
            self.day = self.configs_json['day']
        else:
            self.policy = self.configs.get('CONFIGS', 'Policy')
            self.trash = self.configs.get('CONFIGS', 'Path')
            self.size = self.configs.get('CONFIGS', 'Size')
            self.day = self.configs.get('CONFIGS', 'Day')

        self.list_old = os.path.join(self.trash, "_____111_1")

        if self.size != size and size != self.SIZE:
            self.size = size

        if self.day != day and day != self.DAY:
            self.day = day

        if self.policy != policy and policy != self.POLICY:
            self.policy = policy

    def create_configs(self, policy=POLICY,path=PATH,
                       size=SIZE, day=DAY):

        if not os.path.exists(self.path_rm_conf_json):
            json.dump(self.standart_configs, open(self.path_rm_conf_json, 'w'))

        if not os.path.exists(self.path_conf_cfg):
            self.configs.add_section('CONFIGS')
            self.configs.set('CONFIGS', 'Policy', policy)
            self.configs.set('CONFIGS', 'Path', path)
            self.configs.set('CONFIGS', 'Size', size)
            self.configs.set('CONFIGS', 'Day', day)
            with open(self.path_conf_cfg, 'w') as configfile:
                self.configs.write(configfile)

    def write_configs(self, new_path, new_policy, new_size, new_day):

        if new_path is not None:
            self.configs.set('CONFIGS', 'Path', os.path.join(os.path.abspath(new_path), 'trash'))
            self.configs_json['path'] = os.path.join(new_path, 'trash')

        if new_policy is not None:
            self.configs.set('CONFIGS', 'Policy', new_policy)
            self.configs_json['policy'] = new_policy;

        if new_size is not None:
            self.configs.set('CONFIGS', 'Size', new_size)
            self.configs_json['size'] = new_size;

        if new_day is not None:
            self.configs.set('CONFIGS', 'Day', new_day)
            self.configs_json['day'] = new_day;

        with open(self.path_conf_cfg, 'w') as configfile:
            self.configs.write(configfile)

        json.dump(self.configs_json, open(self.path_rm_conf_json, 'w'))
