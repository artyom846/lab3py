WEBMYRMtool 0.5
Small UNIX OS utilite providing removing/restoring opeartions with file storage support. Uses web interface.
Developed on python 2.7 & Django 1.11.4

Requirements:

1) To install this piece of software, run:
    pip install MySQL-python
2) Create MySQL database for your project

Usage guide:
To run server, type in webrm project folder:
    python manage.py runserver
To activate web-interface, type
in your web-browser adress input.